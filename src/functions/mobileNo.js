class MobileNo {

    validate(mobileNo) {
        return ((/^0[0-9]{9}$/).test(mobileNo) == true) ? 'success' : 'mobileNo is incorrect'
    }
}

module.exports = MobileNo