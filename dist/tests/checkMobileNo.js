'use strict';

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _mobileNo = require('../functions/mobileNo');

var _mobileNo2 = _interopRequireDefault(_mobileNo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expect = _chai2.default.expect;

describe('Check MobileNo', function () {

  //test case null
  it('fail when mobileNo = null, expect return mobileNo is incorrect', function () {
    var mobileNo = null;
    var result = new _mobileNo2.default().validate(mobileNo);
    expect(result).to.equal('mobileNo is incorrect');
  });
  //test case space
  it('fail when mobileNo = \"\", expect return mobileNo is incorrect', function () {
    var mobileNo = "";
    var result = new _mobileNo2.default().validate(mobileNo);
    expect(result).to.equal('mobileNo is incorrect');
  });
  //test case charector
  it('fail when mobileNo = 0123456Abc, expect return mobileNo is incorrect', function () {
    var mobileNo = "0123456Abc";
    var result = new _mobileNo2.default().validate(mobileNo);
    expect(result).to.equal('mobileNo is incorrect');
  });
  //test case special character
  it('fail when mobileNo = 0123456#&@, expect return mobileNo is incorrect', function () {
    var mobileNo = '0123456#&@';
    var result = new _mobileNo2.default().validate(mobileNo);
    expect(result).to.equal('mobileNo is incorrect');
  });
  //test case length ≠ 10
  it('fail when mobileNo = 012345678, expect return mobileNo is incorrect', function () {
    var mobileNo = '012345678';
    var result = new _mobileNo2.default().validate(mobileNo);
    expect(result).to.equal('mobileNo is incorrect');
  });

  //test case mobileNo[0] ≠ 0
  it('fail when mobileNo = 9123456789, expect return mobileNo is incorrect', function () {
    var mobileNo = '9123456789';
    var result = new _mobileNo2.default().validate(mobileNo);
    expect(result).to.equal('mobileNo is incorrect');
  });

  //test case success
  it('success when mobileNo = 0812345678, expect return success', function () {
    var mobileNo = '0812345678';
    var mobiemobileNo = new _mobileNo2.default().validate(mobileNo);
    expect(mobiemobileNo).to.equal('success');
  });
});